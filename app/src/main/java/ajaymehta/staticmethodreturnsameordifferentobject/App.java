package ajaymehta.staticmethodreturnsameordifferentobject;

/**
 * Created by Avi Hacker on 7/22/2017.
 */

public class App {

    public static void main(String args[]) {

        System.out.println(new One().method()+ " --- "+ new Two().method());
        System.out.println(new One().method2());


        // making a method static doesnt mean ..its gonna return the same object...
        // it still returns differnt objects....
        // its a POJO Pitfall ...you can't get object in diffent classes..
        // every time you call a static method .it returns a differnt object ..even if you are in same class or differnt class or even in same classi n same or different method.
    }

}

 class One {

     public Movie method() {

         Movie obj1 = Movie.getMoiveObject();
         return obj1;
     }

     public Movie method2() {

         Movie obj3 = Movie.getMoiveObject();
         return obj3;
     }
 }

class Two {

    public Movie method() {

        Movie obj2 = Movie.getMoiveObject();
        return obj2;
    }
}