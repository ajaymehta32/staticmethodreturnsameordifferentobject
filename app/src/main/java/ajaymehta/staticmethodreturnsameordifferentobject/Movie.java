package ajaymehta.staticmethodreturnsameordifferentobject;

/**
 * Created by Avi Hacker on 7/22/2017.
 */

public class Movie {

    private String movieName;

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public static Movie getMoiveObject() {

        Movie movie = new Movie();

        return movie;

    }

}
